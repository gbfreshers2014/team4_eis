/*jslint anon:true, sloppy:true, nomen:true*/
YUI.add('attendancetasks-binder-index', function(Y, NAME) {

/**
 * The attendancetasks-binder-index module.
 *
 * @module attendancetasks-binder-index
 */

    /**
     * Constructor for the AttendanceTasksBinderIndex class.
     *
     * @class AttendanceTasksBinderIndex
     * @constructor
     */
    Y.namespace('mojito.binders')[NAME] = {

        /**
         * Binder initialization method, invoked after all binders on the page
         * have been constructed.
         */
        init: function(mojitProxy) {
            this.mojitProxy = mojitProxy;
        },

        /**
         * The binder method, invoked to allow the mojit to attach DOM event
         * handlers.
         *
         * @param node {Node} The DOM node to which this mojit is attached.
         */
        bind: function(node) {
            var me = this;
            this.node = node;

            var calendar = new Y.Calendar({
              contentBox: "#mycalendar",
              width:'340px',
              showPrevMonth: true,
              showNextMonth: true,
              date: new Date()}).render();
            
            // Get a reference to Y.DataType.Date
            var dtdate = Y.DataType.Date;

            // Listen to calendar's selectionChange event.
            calendar.on("selectionChange", function (ev) {

              // Get the date from the list of selected
              // dates returned with the event (since only
              // single selection is enabled by default,
              // we expect there to be only one date)
              var newDate = ev.newSelection[0];

              // Format the date and output it to a DOM
              // element.
              //Y.one("#datevalue").setHTML(dtdate.format(newDate));
              Y.one("#datevalue").set('value',dtdate.format(newDate));
            });
            /**
             * Example code for the bind method:
             *
             * node.all('dt').on('mouseenter', function(evt) {
             *   var dd = '#dd_' + evt.target.get('text');
             *   me.node.one(dd).addClass('sel');
             *
             * });
             * node.all('dt').on('mouseleave', function(evt) {
             *   
             *   var dd = '#dd_' + evt.target.get('text');
             *   me.node.one(dd).removeClass('sel');
             *
             * });
             */
        }

    };

}, '0.0.1', {requires: ['event-mouseenter', 'mojito-client', 'calendar']});
