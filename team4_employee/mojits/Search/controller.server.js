YUI.add('search', function(Y, NAME) {

    Y.namespace('mojito.controllers')[NAME] = {
        index: function(ac) {
            ac.models.get('model').getData(function(err, data) {
                if (err) {
                    ac.error(err);
                    return;
                }
                ac.assets.addCss('./index.css');
                ac.done({
                    status: 'Mojito is working.',
                    data: data
                });
            });
        },
        result: function(ac) {

            ac.models.get('model').searchResult(ac,function(err,res) {

                var employee_list = [];

                //if admin
                if(ac._adapter.req.user.role=="admin") {

                    for (var i = 0; i < res.emplist.length; i++) {

                        res.emplist[i].personalDetails.sno = i + 1;
                        res.emplist[i].personalDetails.type = true;
                        employee_list.push(res.emplist[i].personalDetails);
                    }
                }
                else
                {
                    for(var i=0; i<res.emplist.length; i++)
                    {

                        res.emplist[i].sno = i+1;
                        res.emplist[i].type = false;
                        employee_list.push(res.emplist[i].personalDetails);
                    }
                }
                if(ac._adapter.req.user.role=="admin") {
                    ac.done({emplist:employee_list, type: true}, "index");
                }
                else
                  ac.done({emplist:employee_list}, "index");

             });
        }


    };

}, '0.0.1', {requires: ['mojito', 'mojito-assets-addon', 'mojito-models-addon', 'mojito-params-addon']});
